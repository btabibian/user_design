from google.appengine.api import users
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from google.appengine.ext import db
from google.appengine.ext.webapp import template
from google.appengine.api import mail

import StringIO
import json
import logging

from data import ActiveModel
from data import RunModel

import matplotlib.pyplot as plt
import numpy as np

from env.deploy import Deploy
from env.rendezvous import Rendezvous
import env.experiment_data as experiment_data
from env.multiagent_env import MultiAgentEnv
class Experiment(webapp.RequestHandler):
    NUMBER_OF_RUNS=20
    NUMBER_OF_ITERATIONS=150
    def __init__(self, *args, **kwargs):
        super(Experiment, self).__init__(*args, **kwargs)
        self.END_HASH=self.hash_func(-1)
        self.BEGIN_HASH=self.hash_func(0)
        self.xPix = 900
        self.yPix = 600
        self.BOTH_MESSAGE="This is a practice trial, you will try both options and observe the effect of two cases on the same initial state of robots. Scores are not considered in practice trials."
        self.xSize = 3 #inches
        self.ySize=self.xSize/self.xPix*self.yPix
        self.exp_count=self.NUMBER_OF_RUNS
    def get(self):
        user = users.get_current_user()
        if user:
            url = users.create_logout_url("/")
            url_linktext = 'Logout'
            active = ActiveModel.gql("WHERE author = :author",
                author=users.get_current_user()).fetch(1)
            if (active[0].state=="Not Started"):
                batch =self._setup_env(active[0],user)
                active[0].put()
                self.redirect('/experiment?id='+str(self.BEGIN_HASH))
            if (self.request.get("id")==""):
                print 'reached begin'
                self.redirect('/experiment?id='+str(self.BEGIN_HASH))
            elif (self.request.get("id")==str(self.END_HASH)):
                print 'reached end of exp'
                self.response.out.write(template.render('experimentend.html',
                                                        None))
            else:
                id=self.hash_func(int(self.request.get("id")))
                print 'end code', self.END_HASH, 'id: ',id
                conf =RunModel.gql(
                                "WHERE experiment_Id=:expid AND run_num=:rid",
                                expid=active[0].key().id(),
                                rid=id).fetch(1)
                if len(conf)>0:
                    if(conf[0].score>0):
                        print 'reached end, redirecting'
                        self.redirect('/experiment?id='+str(self.END_HASH))
                        return
                both_message=""
                if (active[0].state=="SetUp"):
                    if len(conf)>0:
                        active[0].state2=db.Category("One")
                        batch=json.loads(conf[0].connfiguration)
                    else:
                        self.redirect('/experiment?id='+str(self.END_HASH))
                elif (active[0].state=="Generic"):
                    self.exp_count=len(experiment_data.EXPERIMET_DATA)
                    if id< len(experiment_data.EXPERIMET_DATA) and id>-1:
                        batch = experiment_data.EXPERIMET_DATA[id]
                        active[0].state2=db.Category(batch["type"])
                        if (batch["type"]=="Both"):
                            both_message=self.BOTH_MESSAGE
                    else:
                        self.redirect('/experiment?='+str(self.END_HASH))
                        return
                active[0].put()
                pic=self.show_plot(batch)

                values = {'user': user,
                'image': pic,
                'deploy': True,
                'rend': True,
                'both_message':both_message,
                'next': self.hash_func(id+1),
                'current':self.hash_func(id),
                'pre': self.hash_func(id-1),
                'total':self.exp_count,
                'exp_count':self.hash_func(id+1),
                'url':url,
                'score':0,
                'url_linktext':url_linktext}
                self.response.out.write(template.render('experiment.html',
                                                        values))      
        else:
            self.redirect('/')
    def post(self):
        user = users.get_current_user()
        if user:
            url = users.create_logout_url("/")
            url_linktext = 'Logout'
            active = ActiveModel.gql("WHERE author = :author",
                author=users.get_current_user()).fetch(1)
            if (active[0].state=="SetUp"):
                exp_id=self.hash_func(int(self.request.get("id")))
                batch =RunModel.gql(
                            "WHERE experiment_Id=:expid AND run_num=:rid",
                            expid=active[0].key().id(),
                            rid=exp_id).fetch(1)[0]
                env=self.get_env(batch)
            elif (active[0].state=="Generic"):
                exp_id=self.hash_func(int(self.request.get("id")))
                self.exp_count=len(experiment_data.EXPERIMET_DATA)
                if exp_id< len(experiment_data.EXPERIMET_DATA) and exp_id>-1:
                        env_conf = experiment_data.EXPERIMET_DATA[exp_id]
                else:
                    self.redirect('/experiment?id='+str(self.END_HASH))
                env=self.get_env(env_conf)
                batch = RunModel(experiment_Id=active[0].key().id(),
                         configuration=json.dumps(env.config()),
                         run_num=exp_id)

            if (self.request.get("str")=="d"):
                pic=self.run_deploy(env)
                batch.decision=db.Category("Deploy")
                batch.score=np.asscalar(env.get_total_explored())
                    
            elif (self.request.get("str")=="r"):
                pic=self.run_rend(env)
                batch.decision=db.Category("Rendezvous")
                batch.score=np.asscalar(env.get_total_explored())
            depOp=False
            rendOp=False

            if active[0].state2=="Both":
                both_message=self.BOTH_MESSAGE
                active[0].state2="One"
                if batch.decision=="Deploy":
                    rendOp=True
                else:
                    depOp=True
            else:
                both_message=""
            values = {'user': user,
            'image': pic,
            'next': self.hash_func(exp_id+1),
            'deploy': depOp,
            'rend': rendOp,
            'both_message':both_message,
            'current':self.hash_func(exp_id),
            'total':self.exp_count,
            'exp_count':self.hash_func(exp_id+1),
            'url':url,
            'url_linktext':url_linktext,
            'pre': self.hash_func(exp_id-1),
            'score':str(int(batch.score*100))+"%"}
            active[0].totalscore=active[0].totalscore+batch.score
            active[0].put()
            batch.put()
            self.response.out.write(template.render('experiment.html',
                                                    values))
        else:
             self.redirect('/')
    def _setup_env(self,active_exp,user):
        active_exp.state=db.Category("SetUp")
        lst=[]
        for i in xrange(self.NUMBER_OF_RUNS):
            #dum_object=dict(scale=10 ,w=12,h=[[10,23],[23,43],[34,23]])
            env=MultiAgentEnv(robots_scale=90,wall_scale=300,wall_counts=40,
                              r_count=40)
            run=RunModel(experiment_Id=active_exp.key().id(),
                         configuration=json.dumps(env.config()),run_num=i)
            run.put()
            lst.append(run)
        return lst
    def get_env(self,conf):
        env=MultiAgentEnv()
        env.set_config(conf)
        return env
    def show_plot(self,conf):
        try:
            env=MultiAgentEnv()
            env.set_config(conf)
            plt.hold(False)
            ax=plt.axes(aspect="equal")
            env.set_axes(ax)
            env.run_location(True)
            rv = StringIO.StringIO()
            ax.xaxis.set_ticks(range(-150,151,100))
            ax.yaxis.set_ticks(range(-150,151,100))
            plt.savefig(rv,format="png",bbox_inches='tight',facecolor='w',transparent = True)
            return rv.getvalue().encode("base64").strip()
        finally:
            plt.hold(False)
            plt.clf()

    def run_deploy(self,env):
        try:
            plt.hold(False)
            ax=plt.axes(aspect='equal')
            env.set_axes(ax)
            
            dep=Deploy(env,self.NUMBER_OF_ITERATIONS)
            dep.initial(False)
            dep.update(None)
            rv = StringIO.StringIO()
            ax.xaxis.set_ticks(range(-150,151,100))
            ax.yaxis.set_ticks(range(-150,151,100))
            plt.savefig(rv,format="png",bbox_inches='tight',facecolor='w',transparent = True)
            return rv.getvalue().encode("base64").strip()
        finally:
            plt.hold(False)
            plt.clf()
    def run_rend(self,env):
        try:
            plt.hold(False)
            ax=plt.axes(aspect='equal')
            
            env.set_axes(ax)
            dep=Rendezvous(env,self.NUMBER_OF_ITERATIONS)
            dep.initial(False)
            dep.update(None)
            rv = StringIO.StringIO()
            ax.xaxis.set_ticks(range(-150,151,100))
            ax.yaxis.set_ticks(range(-150,151,100))
            plt.savefig(rv,format="png",bbox_inches='tight',facecolor='w',transparent = True)
            return rv.getvalue().encode("base64").strip()
        finally:
            plt.hold(False)
            plt.clf()
    def setup_axis(self,current_ax):
        current_ax.axis("equal")
        current_ax.set_xlim(self.axis_lim)
        current_ax.set_ylim(self.axis_lim)
        current_ax.set_autoscale_on(False)
        current_ax.set_yticks(range(self.axis_lim[0],self.axis_lim[1]+1,100))
        current_ax.set_xticks(range(self.axis_lim[0],self.axis_lim[1]+1,100))
    def hash_func(self,n):
        return n
            