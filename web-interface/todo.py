from google.appengine.api import users
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from google.appengine.ext import db
from google.appengine.ext.webapp import template
from google.appengine.api import mail
import logging

from data import ActiveModel
from data import RunModel
from experiment import Experiment

class MainPage(webapp.RequestHandler):
    def get(self):
        user = users.get_current_user()
        url = users.create_login_url(self.request.uri)
        url_linktext = 'Login'
                    
        if user:
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'
        active = ActiveModel.gql("WHERE author = :author",
                author=users.get_current_user())
        if (active.count()>0):
            values = {
                'active': active[0],
                'user': user,
                'url': url,
                'url_linktext': url_linktext,
            }
        else:
            values = {
                'user': user,
                'url': url,
                'url_linktext': url_linktext,
            }
        self.response.out.write(template.render('index.html', values))
        
class Done(webapp.RequestHandler):
    def get(self):
        user = users.get_current_user()
        if user:
            self.redirect('/experiment')


class New(webapp.RequestHandler):
    def get(self):
        logging.debug('New Exp requested')
        user = users.get_current_user()
        if user:
            #if not testurl.startswith("http://") and testurl:
            #    testurl = "http://"+ testurl
            active = ActiveModel.gql("WHERE author = :author",
                author=users.get_current_user()).fetch(1)
            if (len(active)>0):
                self.redirect('/')
                return 
            active = ActiveModel(
                    author  = users.get_current_user(),
                    state=db.Category("Generic"),
                    finished = False,totalscore=0.0)
            active.put();
            logging.debug('Created new Exp')
            self.redirect('/')           


# Register the URL with the responsible classes
app = webapp.WSGIApplication([('/', MainPage),
                              ('/new', New),
              ('/done', Done),('/experiment', Experiment)],#
              debug=True)

# Register the wsgi application to run
def main():
  run_wsgi_app(application)

if __name__ == "__main__":
  main() 