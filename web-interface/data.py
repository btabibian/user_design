from google.appengine.api import users
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from google.appengine.ext import db
from google.appengine.ext.webapp import template
from google.appengine.api import mail

class ActiveModel(db.Model):
    author      = db.UserProperty(required=True)
    created          = db.DateTimeProperty(auto_now_add=True)
    updated      = db.DateTimeProperty(auto_now=True)
    state         = db.CategoryProperty()
    totalscore=db.FloatProperty()
    state2         = db.CategoryProperty()

class RunModel(db.Model):
    experiment_Id=db.IntegerProperty(required=True)
    run_num=db.IntegerProperty(required=True)
    configuration=db.TextProperty()
    decision=db.CategoryProperty()
    score=db.FloatProperty()
    created = db.DateTimeProperty(auto_now_add=True)

    def __repr__(self):
        return str(self.run_num)+"--"+str(self.configuration)