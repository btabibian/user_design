# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

#%reset

import numpy as np
import random
import math
import datetime

from matplotlib.patches import Polygon
from matplotlib.patches import Circle
from matplotlib.widgets import Button
from matplotlib.lines import Line2D
from multiagent_env import MultiAgentEnv
from voronoi import Voronoi
from abstractStrategy import Strategy
import Utils
import matplotlib.pyplot as plt
import experiment_data
# <codecell>

class Rendezvous(Strategy):
    def __init__(self,env,iteration=1):
        super(Rendezvous, self ).__init__(env,iteration)
        self.thresh=900
    def algorithm(self):
        matrix=self.adj_matrix(self.env.get_robots_location(),self.thresh)
        dest=self.find_dest(matrix,self.env.get_robots_location())
        new_dir= self.constraint_matching(self.env.get_robots_location(),
                                          matrix,dest,self.thresh)
        return new_dir
    def post_batch(self,current_ax):
        #matrix=self.adj_matrix(self.env.get_robots_location(),self.thresh)
        #self.draw_adj_mat(matrix,self.env.get_robots_location(),current_ax)
        pass
    def draw_adj_mat(self,adj_matrix,poses,axis):
        for i in xrange(adj_matrix.shape[0]):
            for point_2 in poses[adj_matrix[i]]:
                points=np.array([poses[i],point_2])
                line=Line2D(points[:,0],points[:,1],linewidth=2)
                axis.add_line(line)
        
    def find_dest(self,adj_matrix,poses):
        direction=np.zeros((poses.shape[0],2))
        for i in xrange(poses.shape[0]):
            direction[i]=np.mean(poses[adj_matrix[i]][:],axis=0)-poses[i]
        return direction
    def adj_matrix(self,poses,thresh):
        mat=np.zeros((poses.shape[0],poses.shape[0]),dtype='bool')
        for i in xrange(poses.shape[0]):
            for j in xrange(i,poses.shape[0]):
                if(Utils.distance_L2(poses[i],poses[j])<thresh):
                    mat[i][j]=True
                    mat[j][i]=True
        return mat
    def constraint_matching(self,points,adj_matrix,directions,thresh):
        new_dir=directions.copy()
        for point_dir_neig in zip(points,directions,
                                  adj_matrix,xrange(adj_matrix.shape[0])):
            neigh=self.env.get_robots_location()[point_dir_neig[2]][:]
            viol= self.check_cons(point_dir_neig[0],neigh,point_dir_neig[1],
                                  thresh)
            if (neigh[viol].shape[0]>0):
                print 'violation'
                new_dir[point_dir_neig[3],:]=0
        return new_dir
    def check_cons(self,point,neig,direction,thresh):
        return Utils.distance_L2(np.zeros((neig.shape[0],2)),
                                 (point+direction)-neig)>thresh

# <codecell>

if __name__=="__main__":
    axprev = plt.axes([0.87, 0.01, 0.1, 0.087])
    bnext = Button(axprev, 'Next')
    env=MultiAgentEnv(robots_scale=90,wall_scale=300,wall_counts=40,r_count=40)
    env.set_config(experiment_data.EXPERIMET_DATA[5])
    dep=Rendezvous(env,iteration=300)
    current_ax=plt.subplot(3,3,1,aspect='equal')
    env.set_axes(current_ax)
    dep.initial()
    def click(event):
        print env.get_total_explored()
        current_ax=plt.subplot(3,3,dep.nCalls,aspect='equal')
        dep.env.set_axes(current_ax)
        dep.update(event)
    _=bnext.on_clicked(click)
    plt.show()

# <codecell>


