# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <markdowncell>

# This script produces the experiment maps
# ----

# <codecell>

%reset
from multiagent_env import MultiAgentEnv
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import random
import shutil
import json

# <markdowncell>

# The environment size is assumed to be [-150,150] in both $x$ and $y$ and maximum speed of robots 1 in $x$ or $y$ direction.
# 
# scale parameres set the variance of the gaussians for robots or walls.
# 
# $X~N(\mu=(r_x,r_y),\Sigma=100*r_{scale}*I)$
# 
# If any element goes beyond the boundary of the environment it will be set to te margin.

# <codecell>

r_scale=[10,210]
w_scale=[10,210]
r_x=[-140,140]
r_y=[-140,140]
w_x=[-140,140]
w_y=[-140,140]
w_count=20
sample_size=150
grid_scale=15

# <markdowncell>

# We uniformly sample from the parameter space.

# <codecell>

robot_scales=np.random.uniform(r_scale[0],r_scale[1],size=sample_size)
wall_scales=np.random.uniform(w_scale[0],w_scale[1],size=sample_size)
robot_x=np.random.uniform(r_x[0],r_x[1],size=sample_size)
robot_y=np.random.uniform(r_y[0],r_y[1],size=sample_size)
wall_x=np.random.uniform(w_x[0],w_x[1],size=sample_size)
wall_y=np.random.uniform(w_y[0],w_y[1],size=sample_size)
complete=np.array([robot_x,robot_y,wall_x,wall_y,robot_scales,wall_scales]).T
dataset=pd.DataFrame(complete,
                     index=xrange(sample_size),columns=['robots_mean_x',
                                                        'robots_mean_y',
                                                        'walls_mean_x',
                                                        'walls_mean_y',
                                                        'robots_scale',
                                                        'wall_scale'])
dataset['show_both']=False
dataset['unsampled']=False

# <markdowncell>

# For every parameter three regions are removed from the sampled space and kept for the last stage of the experiment when the subject has interacted with the environment.
# 
# Number experiments from sampled space is 50
# 
# Number of experiments for testing of not sampled space is  10
# 
# Four datasets are produced:
# 
# unsampled, 10 experiments with parameters sampled from the specific regions
# 
# both_set, first 10 experiments which results of both actions will be shown to the subject.
# 
# repeated, next 10 experiments that will be shown randomly again later when user finished main experiments.
# 
# main_sampled, remaining experiments for the two-choice forced task.

# <codecell>

unsampled_r_x_up=(dataset.robots_mean_x<130) & (dataset.robots_mean_x>120)
unsampled_r_y_up=(dataset.robots_mean_y<130) & (dataset.robots_mean_y>120)
unsampled_r_x_down=(dataset.robots_mean_x>-130) & (dataset.robots_mean_x<-120)
unsampled_r_y_down=(dataset.robots_mean_y>-130) & (dataset.robots_mean_y<-120)
unsampled_r_x_mid=(dataset.robots_mean_x<10) & (dataset.robots_mean_x>0)
unsampled_r_y_mid=(dataset.robots_mean_y<10) & (dataset.robots_mean_y>0)


unsampled_w_x_up=(dataset.walls_mean_x<130) & (dataset.walls_mean_x>120)
unsampled_w_y_up=(dataset.walls_mean_y<130) & (dataset.walls_mean_y>120)
unsampled_w_x_down=(dataset.walls_mean_x>-130) & (dataset.walls_mean_x<-120)
unsampled_w_y_down=(dataset.walls_mean_y>-130) & (dataset.walls_mean_y<-120)
unsampled_w_x_mid=(dataset.walls_mean_x<10) & (dataset.walls_mean_x>0)
unsampled_w_y_mid=(dataset.walls_mean_y<10) & (dataset.walls_mean_y>0)

unsampled_r_s_up=(dataset.robots_scale<330) & (dataset.robots_scale>320)
unsampled_r_s_down=(dataset.robots_scale>10) & (dataset.robots_scale<20)
unsampled_r_s_mid=(dataset.robots_scale<190) & (dataset.robots_scale>170)

unsampled_w_s_up=(dataset.wall_scale<330) & (dataset.wall_scale>320)
unsampled_w_s_down=(dataset.wall_scale>10) & (dataset.wall_scale<20)
unsampled_w_s_mid=(dataset.wall_scale<190) & (dataset.wall_scale>170)
dataset['unsampled']=0
dataset['unsampled'][unsampled_r_x_up | unsampled_r_y_up | unsampled_r_x_down | unsampled_r_y_down | unsampled_r_x_mid | unsampled_r_y_mid]=True
dataset['unsampled'][unsampled_r_s_up | unsampled_r_s_down |  unsampled_r_s_mid ]=True
dataset['unsampled'][unsampled_w_x_up | unsampled_w_y_up | unsampled_w_x_down | unsampled_w_y_down | unsampled_w_x_mid | unsampled_w_y_mid]=True
dataset['unsampled'][unsampled_w_s_up | unsampled_w_s_down |  unsampled_w_s_mid ]=True

sampled=dataset[dataset['unsampled']==False]
sampled=sampled.ix[sampled.index[0:50]]

unsampled=dataset[dataset['unsampled']==True]
unsampled=unsampled.ix[unsampled.index[0:10]]

indeces=sampled.index[0:50]+unsampled.index[0:10]

both_set=sampled[0:10]

repeated=sampled[10:20]

main_sampled=sampled[20:]

# <markdowncell>

# Produce the actual map objects

# <codecell>

def produce_map(x):
    env=MultiAgentEnv(robots_scale=x['robots_scale'],exploration_map_scale=grid_scale,
                      wall_scale=x['wall_scale'],
                      robot_mean=[x['robots_mean_x'],x['robots_mean_y']],
                      wall_mean=[x['walls_mean_x'],x['walls_mean_y']],wall_counts=w_count)
    return env
show_both_maps=both_set.apply(lambda x: produce_map(x),axis=1)
sampled_maps=main_sampled.apply(lambda x: produce_map(x),axis=1)
unsampled_maps=unsampled.apply(lambda x: produce_map(x),axis=1)
repeated_maps=repeated.apply(lambda x: produce_map(x),axis=1)

# <markdowncell>

# Shuffling repeated maps

# <codecell>

repeated_maps_list=repeated_maps.tolist()
random.shuffle(repeated_maps_list)

# <markdowncell>

# Printing to file

# <codecell>

file=open('experiment_data.py','w')
file.write('EXPERIMET_DATA=[')
_=map(file.write,map((lambda x:json.dumps(x.config())+',\n'), show_both_maps))

_=map(file.write,map((lambda x:json.dumps(x.config())+',\n'), sampled_maps))

_=map(file.write,map((lambda x:json.dumps(x.config())+',\n'), (j for i in zip(repeated_maps_list,unsampled_maps) for j in i)))
file.write(']')
shutil.copyfile('./experiment_data.py','../outputs/experiment_data.py')

# <codecell>

file.close()

