# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

#%reset

import numpy as np
import random
import math
import datetime
import sys
import time

from matplotlib.patches import Polygon
from matplotlib.patches import Circle
from matplotlib.widgets import Button
from matplotlib.lines import Line2D
from multiagent_env import MultiAgentEnv
from voronoi import Voronoi
from abstractStrategy import Strategy
import Utils
import matplotlib.pyplot as plt
import experiment_data
# <codecell>

class Deploy(Strategy):
    def __init__(self,env,iteration=1):
        super(Deploy, self ).__init__(env,iteration)
        self.att_force=100
    
    def algorithm(self):
        
        update_vec=np.zeros((self.env.get_robots_location().shape[0],2))
        model=Voronoi(self.env.get_robots_location()[:,0],
                      self.env.get_robots_location()[:,1])
        model.compute()
        centers=model.compute_centers()
        for i in xrange(centers.shape[0]):
            if (not math.isnan(centers[i][0])) and \
                (not math.isnan(centers[i][1])) :
                att_x=(self.env.get_robots_location()[i][0]-centers[i][0])
                att_y=(self.env.get_robots_location()[i][1]-centers[i][1])
                dif_x= min([att_x,self.att_force])
                dif_y= min([att_y,self.att_force])
                dif_x= max([dif_x,-1*self.att_force])
                dif_y= max([dif_y,-1*self.att_force])
                update_vec[i]=-0.1*(np.array([dif_x,dif_y]))
        return update_vec
    def post_batch(self,draw):
        if(draw):
            model=Voronoi(self.env.get_robots_location()[:,0],
                          self.env.get_robots_location()[:,1])
            model.compute()
            #model.draw_voronoi(self.env.axes)

# <codecell>

if __name__=="__main__":
    np.set_printoptions(threshold=sys.maxint)
    axprev = plt.axes([0.87, 0.01, 0.1, 0.087])
    bnext = Button(axprev, 'Next')
    env=MultiAgentEnv(robots_scale=90,wall_scale=300,wall_counts=40,
                      r_count=40)
    env.set_config(experiment_data.EXPERIMET_DATA[4])
    env.shake()
    dep=Deploy(env,iteration=11)
    state=1
    current_ax=plt.subplot(3,3,state,aspect='equal')
    env.set_axes(current_ax)
    current_ax.xaxis.set_ticks(range(-150,151,50))
    dep.initial()
    def click(event):
        print env.get_total_explored()
        current_ax=plt.subplot(3,3,dep.nCalls,aspect='equal')
        dep.env.set_axes(current_ax)
        dep.update(event)
    _=bnext.on_clicked(click)
    plt.show()
