# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

import numpy as np
def distance_L2(x,y):
    return np.sum(np.abs(y-x)**2,axis=-1)**(1./2)

