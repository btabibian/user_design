# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.patches import Circle

class Voronoi:
    def __init__(self,X,Y):
        self.X=X
        self.Y=Y
        self.count=X.shape[0]
        self.segments=None
        self.index_by_point=None
        self.C=None
        
    @staticmethod
    def circumcircle(P1,P2,P3):
        ''' 
        Adapted from:
        http://local.wasp.uwa.edu.au/~pbourke/geometry/circlefrom3/Circle.cpp
        '''
        delta_a = P2 - P1
        delta_b = P3 - P2
        if np.abs(delta_a[0]) <= 0.000000001 and np.abs(delta_b[1]) <= \
                                                 0.000000001:
            center_x = 0.5*(P2[0] + P3[0])
            center_y = 0.5*(P1[1] + P2[1])
        else:
            aSlope = delta_a[1]/delta_a[0]
            bSlope = delta_b[1]/delta_b[0]
            if np.abs(aSlope-bSlope) <= 0.000000001:
                return None
            center_x= (aSlope*bSlope*(P1[1] - P3[1]) + \
                        bSlope*(P1[0] + P2 [0]) -
                       aSlope*(P2[0]+P3[0]) )/(2* (bSlope-aSlope) )
            center_y = -1*(center_x - (P1[0]+P2[0])/2)/aSlope + \
            (P1[1]+P2[1])/2;
        return center_x, center_y

    def compute(self):
        X=self.X
        Y=self.Y
        P = np.zeros((X.size+8,2))
        P[:X.size,0], P[:Y.size,1] = X, Y
        # We add four points at "infinity"
        m = max(np.abs(X).max(), np.abs(Y).max())*1e4
        P[X.size:,0] = -m, -m, +m, +m,-m/2.0,-m/2.0,+m/2.0,+m/2.0
        P[Y.size:,1] = -m, +m, -m, +m,-m/2.0,+m/2.0,-m/2.0,+m/2.0
        D = matplotlib.tri.Triangulation(P[:,0],P[:,1])
        T = D.triangles
        n = T.shape[0]
        C = np.zeros((n,2))
        for i in range(n):
            C[i] = Voronoi.circumcircle(P[T[i,0]],P[T[i,1]],P[T[i,2]])
        index_by_point=np.zeros((n,4))
        for i in range(n):
            index_by_point[i][0]=i
            index_by_point[i][1]=T[i,0]
            index_by_point[i][2]=T[i,1]
            index_by_point[i][3]=T[i,2]
        X,Y = C[:,0], C[:,1]
        segments = []
        for i in range(n):
            for j in range(3):
                k = D.neighbors[i][j]
                if k != -1:
                    segments.append( [(X[i],Y[i]), (X[k],Y[k])] )
        self.segments=segments
        self.index_by_point=index_by_point
        self.C=C
        return

    def compute_centers(self):
        center=np.zeros((self.count,2))
        for i in xrange(self.count):
            index=self.index_by_point[np.logical_or.reduce(
                            [self.index_by_point[:,j] == i for j in [1,2,3]])]
            active=self.C[index[:,0].astype(int)]
            center[i]=np.mean(active,axis=0)
        return center
    def draw_voronoi(self,ax):
         lines = matplotlib.collections.LineCollection(self.segments,
                                                       color='0.75')
         ax.add_collection(lines)
            
if __name__ == '__main__':
    count=100
    X = np.random.random(count)*10
    Y = np.random.random(count)*10
    fig = plt.figure()
    axes = plt.subplot(1,1,1)
    plt.scatter(X,Y)
    vornoi = Voronoi(X,Y)
    vornoi.compute()
    vornoi.draw_voronoi(axes)
    center=vornoi.compute_centers()
    for i in xrange(count):
        p=Circle(center[i],0.01,color='g')
        axes.add_patch(p)
        p=Circle([X[i],Y[i]],0.01,fill=False)
        axes.add_patch(p)
    plt.show()

