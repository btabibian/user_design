# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

import numpy as np
import random
import math
import datetime

from matplotlib.patches import Polygon
from matplotlib.patches import Circle
from matplotlib.widgets import Button
from matplotlib.lines import Line2D
from voronoi import Voronoi
import Utils
import matplotlib.pyplot as plt

# <codecell>

class MultiAgentEnv:
    def __init__(self,robots_scale=3,wall_scale=100,wall_counts=40,
                 exploration_map_scale=10,exploration_map_bias=[150,150],
                 r_count=20,robot_mean=[0,0],wall_mean=[0,0]):
        self.scale=100
        self.w=.6*self.scale*0.05
        self.h=3.0*self.scale*0.05
        self.mean=wall_mean
        self.var=wall_scale*self.scale
        self.cov=[[self.var,0],[0,self.var]]
        self.count=wall_counts
        self.axes=None
        self.r=0.8*self.scale*0.05
        self.ln=0.9*self.scale*0.05
        self.r_count=r_count
        self.axis_lim=[-155,155]
        self.r_mean=robot_mean
        self.r_var=robots_scale*self.scale
        self.r_cov=[[self.r_var,0],[0,self.r_var]]
        self.margin=2
        
        self.room=[-150,150,150,-150]
        self.exploration_map_scale=exploration_map_scale
        self.exploration_map_bias=exploration_map_bias
        
        self.setup()
    def setup_axis(self,current_ax):
        #current_ax.set_autoscale_on(False)
        current_ax.set_xlim(self.axis_lim)
        current_ax.set_ylim(self.axis_lim)
        #current_ax.axis("equal")
        current_ax.set_xbound(self.axis_lim[0],self.axis_lim[1])
        current_ax.set_yticks(range(self.axis_lim[0],self.axis_lim[1]+1,100))
        current_ax.set_xticks(range(self.axis_lim[0],self.axis_lim[1]+1,100))

    def check_boundaries(self,points,margin=-1):
        if (margin==-1):
            margin=self.margin
        for i in points:
            if i[0]<self.room[0]:
                i[0]=self.room[0]+margin
            if i[1]<self.room[3]:
                i[1]=self.room[3]+margin
            if i[0]>self.room[2]:
                i[0]=self.room[2]-margin
            if i[1]>self.room[1]:
                i[1]=self.room[1]-margin

    def setup(self,conf=False):
        if(conf==False):
            self.points=np.random.multivariate_normal(self.mean,self.cov,
                                                      self.count)
            self.hor=np.random.binomial(1,0.5,self.count)
            self.r_points=np.random.multivariate_normal(self.r_mean,
                                                        self.r_cov,
                                                        self.r_count)
            self.r_direction=np.random.uniform(0,2*3.14,self.r_count)

            self.check_boundaries(self.r_points)
            self.check_boundaries(self.points)
            self.init_wpoints=self.points
            self.init_w_hor=self.hor
            self.init_r_points=self.r_points
            self.init_r_direction=self.r_direction
        else:
            self.points=self.init_wpoints
            self.hor=self.init_w_hor
            self.r_points=self.init_r_points
            self.r_direction=self.init_r_direction
        shape=((self.room[2]-self.room[0])/self.exploration_map_scale+2,
               (self.room[1]-self.room[3])/self.exploration_map_scale+2)
        self.grid=np.zeros(shape,dtype='int')    
        self.w_points=zip(self.points,self.hor)
    def get_total_explored(self):
        return np.mean( self.grid)
    def get_exploration_map(self):
        return self.grid
    def exploration(self):
        indecies= np.floor_divide(self.get_robots_location()+self.exploration_map_bias,
                                  self.exploration_map_scale).astype('int')+1
        #indecies[np.where(indecies[:,0]>=self.grid.shape[0]),0]=self.grid.shape[0]-1
        #indecies[np.where(indecies[:,1]>=self.grid.shape[1]),1]=self.grid.shape[1]-1
        self.grid[indecies[:,0],indecies[:,1]]=1
    def produce_exploration_map(self,map_layout,axis):
        print 'drawing function called'
        k=np.where(map_layout==1)
        for i,j in zip(k[0],k[1]):
            if(map_layout[i,j]==1):
                scale=self.exploration_map_scale
                orig_point=np.array([i*self.exploration_map_scale-
                                     self.exploration_map_bias[0]-
                                     self.exploration_map_scale/2,
                                     j*self.exploration_map_scale-
                                     self.exploration_map_bias[1]-
                                     self.exploration_map_scale/2])
                p= Circle(orig_point,self.exploration_map_scale/1.3, alpha=0.8,color='b')
                axis.add_patch(p)
    def produce_wall(self,location,axis,ver=True):
        width=self.w
        height=self.h
        if(ver==False):
            width=self.h
            height=self.w
        patch=[[location[0]-width/2,location[1]-height/2],
               [location[0]+width/2,location[1]-height/2],
              [location[0]+width/2,location[1]+height/2],
               [location[0]-width/2,location[1]+height/2]]
        pts = np.array(patch)
        p = Polygon(pts, closed=True, alpha=0.35,color='k')
        axis.add_patch(p)
        
    def produce_robot(self,pose,axis):
        p=Circle(pose[:2],self.r,fill=True,color='y')
        points=np.array([pose[:2],pose[:2]+
                         np.array([self.ln*np.sin(pose[2]),
                                   self.ln*np.cos(pose[2])])])
    
        line=Line2D(points[:,0],points[:,1],linewidth=1,color='k')
        axis.add_patch(p)
        axis.add_line(line)
    def update(self,ax):
        print 'drawing function called'
        for pts in self.w_points:
            self.produce_wall(pts[0],ax,pts[1])
        for i in zip(self.r_points,self.r_direction):
            self.produce_robot((i[0][0],i[0][1],i[1]),ax)
    def get_robots_location(self):
        return self.r_points
    def get_robots_heading(self):
        return self.r_direction
    def get_wall_location(self):
        return self.w_points
    def set_axes(self,axes):
        self.axes=axes
        self.setup_axis(self.axes)
    def run_location(self,axis=True,r_points_diff=None,r_direction_diff=None,
                     exploration_map=None):
        if(r_points_diff!=None):
            self.r_points=self.r_points+r_points_diff
            self.check_boundaries(self.r_points,1)
        if (exploration_map!=None)&(axis==True):
            self.produce_exploration_map(exploration_map,self.axes)
        if(r_direction_diff!=None):
            self.r_direction=self.r_direction+r_direction_diff
        if(axis==True):
            self.update(self.axes)
    def shake(self):
            locs=self.r_points
            self.r_points+=np.random.multivariate_normal([0,0],[[1,0],[0,1]],self.r_count)
    def config(self):
        return dict(scale=self.scale ,w=self.w,h=self.h,mean=self.mean,
                    var=self.var,count=self.count,r=self.r,ln=self.ln,
                    r_count=self.r_count, r_mean=self.r_mean,r_var=self.r_var,
                    exploration_map_scale=self.exploration_map_scale,
                    exploration_map_bias=self.exploration_map_bias,
                    init_wpoints=self.init_wpoints.tolist(),
                    init_w_hor=self.init_w_hor.tolist(),
                    init_r_points=self.init_r_points.tolist(),
                    init_r_direction=self.init_r_direction.tolist())
    def set_config(self,dic):
        self.scale=dic['scale']
        self.w=dic['w']
        self.h=dic['h']
        self.mean=dic['mean']
        self.var=dic['var']
        self.count=dic['count']
        self.r=dic['r']
        self.ln=dic['ln']
        self.r_count=dic['r_count']
        self.r_mean=dic['r_mean']
        self.r_var=dic['r_var']
        self.exploration_map_scale=dic['exploration_map_scale']
        self.exploration_map_bias=dic['exploration_map_bias']
        self.init_wpoints=np.array(dic['init_wpoints'])
        self.init_w_hor=np.array(dic['init_w_hor'])
        self.init_r_points=np.array(dic['init_r_points'])
        self.init_r_direction=np.array(dic['init_r_direction'])
        self.setup(True)

