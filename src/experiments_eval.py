# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

%reset
from multiagent_env import MultiAgentEnv
from deploy import Deploy
from rendezvous import Rendezvous
import experiment_data

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import random
import json

# <codecell>

NUMBER_OF_ITERATIONS=150

# <codecell>

dataset=pd.DataFrame(index=xrange(len(experiment_data.EXPERIMET_DATA)),
              columns=['robot_x','robot_y','robot_scale','wall_x','wall_y','wall_scale','Rendezvous','Deploy'])

# <codecell>

for i,j in zip(experiment_data.EXPERIMET_DATA,xrange(len(experiment_data.EXPERIMET_DATA))):
    env=MultiAgentEnv()
    env.set_config(i)
    dataset['robot_x'][j]=env.r_mean[0]
    dataset['robot_y'][j]=env.r_mean[1]
    dataset['robot_scale'][j]=env.r_var
    dataset['wall_x'][j]=env.mean[0]
    dataset['wall_y'][j]=env.mean[1]
    dataset['wall_scale'][j]=env.var
    env=MultiAgentEnv()
    env.set_config(i)
    dep=Rendezvous(env,iteration=NUMBER_OF_ITERATIONS)
    dep.initial(draw=False)
    dep.update(None,draw=False)
    dataset['Rendezvous'][j]=env.get_total_explored()
    env=MultiAgentEnv()
    env.set_config(i)
    dep=Deploy(env,iteration=NUMBER_OF_ITERATIONS)
    dep.initial(draw=False)
    dep.update(None,draw=False)
    dataset['Deploy'][j]=env.get_total_explored()

# <codecell>

rend=dataset[dataset['Rendezvous']>dataset['Deploy']]
dep=dataset[dataset['Rendezvous']<dataset['Deploy']]
print rend.shape
print dep.shape

# <codecell>

dataset.to_csv('../outputs/experiments.csv',sep='\t',index=False)

