# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>


import numpy as np
import random
import time
import math
import datetime

from multiagent_env import MultiAgentEnv
from voronoi import Voronoi
import Utils
import matplotlib.pyplot as plt

# <codecell>

class Strategy(object):
    """ an abstract strategy that implements cycles of running the algorithm"""
    def __init__(self,env,iteration=1,subplot=(0,0),robots_scale=3,wall_s=100,
                 wall_counts=0,with_rep=True,r_count=20):
        self.env=env
        self.nCalls=2
        self.iteration=iteration
        self.with_rep=with_rep
        self.total_time=0
        self.wall_thresh=10
        self.rep_force=30

    def algorithm(self):
        """For every strategy this method should be implemted and return 
       a vector of movements of each robot. """
        return np.zeros((self.env.get_robots_location().shape[0],2))
    def post_batch(self):
        """This function will be called after on batch of the algorithm is
        completed"""
        pass 
    
    def initial(self,draw=True):
        """This method does not run the strategy and only performs drawing
        procedures """
        self.post_batch(draw)
        self.env.exploration()
        self.env.run_location(axis=draw)
    def update(self,event,draw=True):
        """The main fuction that performs the run of the algorithm for the 
        given number of iterations ssepcificied in constrcutur """

        self.total_time=0
        self.nCalls+=1
        update_vec=np.zeros((self.env.get_robots_location().shape[0],2))
        total_move=np.zeros((self.env.get_robots_location().shape[0],2))
        for j in xrange(self.iteration):
            start=time.clock()
            update_vec=self.algorithm()
            if(self.with_rep):
                reps=self.repulsive()
                update_vec=update_vec+reps
            for i in xrange(self.env.get_robots_location().shape[0]):
                dif_x= min([update_vec[i][0],1])
                dif_y= min([update_vec[i][1],1])
                dif_x= max([dif_x,-1])
                dif_y= max([dif_y,-1])
                update_vec[i]=np.array([dif_x,dif_y])
            self.env.run_location(axis=False,r_points_diff=update_vec)
            total_move+=update_vec
            self.env.exploration()
            self.total_time+=time.clock()-start
        self.post_batch(draw)
        print 'average cycle: ',self.total_time/(self.iteration*1.0)
        print total_move.mean(axis=0)
        map_lay= self.env.get_exploration_map()
        self.env.run_location(axis=draw,exploration_map=map_lay)
        
    def repulsive(self):
        """Computes repulsive force for robots close to walls and obstacles"""
        reps=np.zeros((self.env.get_robots_location().shape[0],2))
        for robot,i in zip(self.env.get_robots_location(),xrange(
                                    self.env.get_robots_location().shape[0])):
            for wall in self.env.get_wall_location():
                if Utils.distance_L2(robot,wall[0])<self.wall_thresh:
                    reps[i]=reps[i]+self.rep_force*1.0/((robot-wall[0]))
            if abs(robot[0]-self.env.room[0])<self.wall_thresh:
                    reps[i][0]= reps[i][0]+self.rep_force*1.0/(np.abs(robot[0]
                                          -self.env.room[0])/self.wall_thresh)
            if abs(robot[1]-self.env.room[1])<self.wall_thresh:
                    reps[i][1]= reps[i][1]-self.rep_force*1.0/(np.abs(robot[1]
                                         -self.env.room[0])/self.wall_thresh)
            if abs(robot[0]-self.env.room[1])<self.wall_thresh:
                    reps[i][0]= reps[i][0]-self.rep_force*1.0/(np.abs(robot[0]
                                         -self.env.room[1])/self.wall_thresh)
            if abs(robot[1]-self.env.room[0])<self.wall_thresh:
                    reps[i][1]= reps[i][1]+self.rep_force*1.0/(np.abs(robot[1]
                                          -self.env.room[0])/self.wall_thresh)
        return reps

